package week;

public class Cpu_one{
    public static void main(String[] args) {
        Cpu cpu=new Cpu();
        Cpu.Processor processor=cpu.new Processor();
        System.out.println(processor.getCache());

        Cpu.Ram ram=cpu.new Ram();
        System.out.println(ram.getClockSpeed());
    }
}

class Cpu {
        double price;
        class Processor {
            double cores;
            String manufacturer;

            double getCache() {
                return 4.3;
            }
        }

    protected class Ram{
        double memory;
        String manufacturer;

        double getClockSpeed(){
            return 5.5;
        }
    }

}