package week.day2;

import java.util.Scanner;

public class SwitchDemo {
    public static void main(String[] args) {
         Scanner sc=new Scanner(System.in);
        System.out.println("enter the temp 1 ,2 and 3");
        int input=sc.nextInt();
        switch(input){
            case 1:
                System.out.println("temp is low");
                break;
            case 2:
                System.out.println("temp is medium");
                break;
            case 3:
                System.out.println("temp is high");
                break;
            default:
                System.out.println("invalied input");
        }

    }
}
